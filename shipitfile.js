module.exports = function (shipit) {

    require('shipit-deploy')(shipit);
    require('shipit-npm')(shipit);
    require('shipit-shared')(shipit);

    shipit.initConfig({
        default: {
            repositoryUrl: 'git@bitbucket.org:norkcode/home-api.git',
            keepReleases: 5,
            workspace: '/tmp/quiz-api',
            key: '/Users/maksimnorkin/.ssh/id_rsa.pub',
            deployTo: '/var/www/prod/quiz-api',
            shallowClone: true
        },
        prod: {
            servers: 'root@212.24.106.103'
        }
    });

    shipit.on('deployed', function () {
        shipit.remote('supervisorctl reload');
    });
};