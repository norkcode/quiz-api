var mongoose = require('mongoose');
var mongooseTimestamp = require('mongoose-timestamp');
var mongooseDelete = require('mongoose-delete');
var mongoosePaginate = require('mongoose-paginate');

var quiz = mongoose.Schema({
    title: String,
    questions: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'question'
    }]
});

quiz.plugin(mongooseTimestamp);
quiz.plugin(mongooseDelete);
quiz.plugin(mongoosePaginate);

module.exports = mongoose.model('quiz', quiz);