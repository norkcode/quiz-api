var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var passportLocalMongoose = require('passport-local-mongoose');
var mongooseDelete = require('mongoose-delete');

var account = mongoose.Schema({
    email: String,
    token: String
});

var options = {
    missingPasswordError: 'Wrong password'
};

account.plugin(passportLocalMongoose, options);
account.plugin(timestamps);
account.plugin(mongooseDelete, {overrideMethods: false});

module.exports = mongoose.model('account', account);
