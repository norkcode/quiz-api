var mongoose = require('mongoose');
var mongooseTimestamp = require('mongoose-timestamp');
var mongooseDelete = require('mongoose-delete');
var mongoosePaginate = require('mongoose-paginate');

var question = mongoose.Schema({
    title: String,
    // collection of answers
    answers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'answer'
    }],
    correct: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'answer'
    }
});

question.plugin(mongooseTimestamp);
question.plugin(mongooseDelete);
question.plugin(mongoosePaginate);

module.exports = mongoose.model('question', question);