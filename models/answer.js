var mongoose = require('mongoose');
var mongooseTimestamp = require('mongoose-timestamp');
var mongooseDelete = require('mongoose-delete');
var mongoosePaginate = require('mongoose-paginate');

var answer = mongoose.Schema({
    title: String
});

answer.plugin(mongooseDelete);
answer.plugin(mongooseTimestamp);
answer.plugin(mongoosePaginate);

module.exports = mongoose.model('answer', answer);