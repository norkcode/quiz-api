var express = require('express');
var router = express.Router();
var quiz = require('../models/quiz');
var question = require('../models/question');

router.get('/', function (req, res) {
    quiz.paginate({}, {
        page: req.query.page || 1,
        limit: 20,
        sort: {
            createdAt: -1
        },
        lean: true
    }).then(function (result) {
        res.json(result);
    });
});

router.get('/:id', function (req, res) {
    quiz.findById(req.params.id)
        .populate({
            path: 'questions',
            populate: [{
                path: 'correct',
                model: 'answer'
            }, {
                path: 'answers',
                model: 'answer'
            }]

        })
        .exec(function (err, item) {
            if (err) return res.status(404).json(err);
            res.json(item);
        });
});

router.post('/', function (req, res) {
    quiz.create({
        title: req.body.title,
        questions: req.body.questions
    }, function (err, item) {
        res.json(item);
    })
});

router.put('/:id', function (req, res) {
    quiz.update({_id: req.params.id}, {
        title: req.body.title,
        questions: req.body.questions
    }, function (err) {
        if (err) return res.status(500).json(res);
        res.sendStatus(200);
    })
});

router.delete('/:id', function (req, res) {
    quiz.delete({_id: req.params.id}, function (err) {
        if (err) res.status(404).json(res);
        return res.sendStatus(200);
    })
});

module.exports = router;