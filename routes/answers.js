var express = require('express');
var router = express.Router();
var answer = require('../models/answer');

router.get('/', function (req, res) {
    answer.paginate({}, {
        page: req.query.page || 1,
        limit: 20,
        sort: {
            createdAt: -1
        },
        lean: true
    }).then(function (result) {
        res.json(result);
    });
});

router.post('/', function (req, res) {
    answer.create({
        title: req.body.title
    }, function (err, item) {
        res.json(item);
    })
});

router.put('/:id', function (req, res) {
    answer.findOneAndUpdate({_id: req.params.id}, {title: req.body.title}, function (err, item) {
        if (err) res.status(500).json(err);
        res.sendStatus(200);
    });
});

router.delete('/:id', function (req, res) {
    answer.delete({_id: req.params.id}, function (err) {
        if (err) res.status(500).json(err);
        res.sendStatus(200);
    });
});

module.exports = router;