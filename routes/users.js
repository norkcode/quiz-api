var express = require('express');
var router = express.Router();

var passport = require('passport');
var account = require('../models/account');

/* GET users listing. */
router.get('/', function(req, res, next) {
  return res.send(req.user);
});

router.post('/', function (req, res) {
    account.register(
        new account({
            username: req.body.username
        }),
        req.body.password,
        function (err, account) {
            if (err) res.status(400).send(err);
            passport.authenticate('local')(req, res, function () {
                res.json(account);
            });
        }
    );
});

router.post('/login', function (req, res) {
    passport.authenticate('local', function (err, user, info) {
        if (err) res.send(err);

        if (!user) {
          return res.sendStatus(403);
        }

        req.logIn(user, function (err) {
          if (err) throw err;

          if (user.deleted) {
            return res.sendStatus(403);
          }

          return res.sendStatus(200);
        });
    })(req, res);
});

module.exports = router;
