var express = require('express');
var router = express.Router();
var question = require('../models/question');

router.get('/', function (req, res) {
    question.paginate({}, {
        page: req.query.page || 1,
        limit: 20,
        sort: {
            createdAt: -1
        },
        lean: true
    }).then(function (result) {
        res.json(result);
    });
});

router.get('/:id', function (req, res) {
    question
        .findOne({_id: req.params.id})
        .populate(['correct', 'answers'])
        .exec(function (err, item) {
            if (err) res.status(500).json(err);
            res.json(item);
        });

});

router.post('/', function (req, res) {
    question.create(
        {
            title: req.body.title,
            answers: req.body.answers,
            correct: req.body.correct
        },
        function (err, item) {
            if (err) return res.status(400).json(err);
            return res.json(item);
        }
    );
});

router.put('/:id', function (req, res) {
    question.findOneAndUpdate(
        {
            _id: req.params.id
        },
        {
            title: req.body.title,
            answers: req.body.answers,
            correct: req.body.correct
        },
        function (err, item) {
            if (err) return res.status(400).json(err);
            return res.sendStatus(200);
        }
    );

});

router.delete('/:id', function (req, res) {
    question.delete({_id: req.params.id}, function (err) {
        if (err) return res.status(500).json(err);
        res.sendStatus(200);
    });
});

module.exports = router;