const quiz = require('../models/quiz');

const chai = require('chai');
const chaiHttp = require('chai-http');
const async = require('async');
const server = require('../app');
const expect = chai.expect;


chai.use(chaiHttp);
chai.config.includeStack = true;

describe('Quiz API', function () {
    beforeEach(function (done) {
        quiz.remove({}, function (err) {
            quiz.create({
                title: 'Test question'
            }, function (err) {
                done();
            });
        });
    });

    it('should return quizzes', function (done) {
        chai.request(server)
            .get('/quizzes')
            .end(function(err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('docs');
                expect(res.body.docs).to.not.be.empty;
                done();
            });
    });

    it('should return no answers on empty page', function (done) {
        chai.request(server)
            .post('/quizzes')
            .send({
                title: 'Quiz title 1'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                done();
            })
    });
    it('should create and edit quiz', function (done) {
        chai.request(server)
            .post('/quizzes')
            .send({
                title: 'Quiz title 4'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('_id');
                chai.request(server)
                    .delete('/quizzes/'+res.body._id)
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        done();
                    })
            });
    });

    it('should create and edit quiz', function (done) {
        chai.request(server)
            .post('/quizzes')
            .send({
                title: 'Quiz title 2',
                questions: []
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('_id');
                expect(res.body).to.have.property('title');
                chai.request(server)
                    .put('/quizzes/' + res.body._id)
                    .send({
                        title: 'Quiz title 3',
                        questions: []
                    })
                    .end(function (err, res2) {
                        expect(res2).to.have.status(200);
                        chai.request(server)
                            .get('/quizzes/' + res.body._id)
                            .end(function (err, res3) {
                                expect(res3).to.have.status(200);
                                expect(res3.body).to.have.property('title', 'Quiz title 3');
                                done();
                            });

                    });

            });
    });

    it('should create a quiz and update questions', function (done) {
        async.parallel({
            questionOne: function (cb) { chai.request(server).post('/questions').send({title: 'Question title 10'}).end(cb); },
            questionTwo: function (cb) { chai.request(server).post('/questions').send({title: 'Question title 11'}).end(cb); },
            quizOne: function (cb) { chai.request(server).post('/quizzes').send({title: 'Quiz title 10'}).end(cb); }
        }, function (err, results) {
            expect(results.questionOne).to.have.status(200);
            expect(results.questionOne.body).to.have.property('title', 'Question title 10');
            expect(results.questionTwo).to.have.status(200);
            expect(results.questionTwo.body).to.have.property('title', 'Question title 11');
            expect(results.quizOne).to.have.status(200);
            expect(results.quizOne.body).to.have.property('title', 'Quiz title 10');

            chai.request(server)
                .put('/quizzes/'+results.quizOne.body._id)
                .send({
                    title: results.quizOne.body.title,
                    questions: [
                        results.questionOne.body._id,
                        results.questionTwo.body._id
                    ]
                })
                .end(function (err, res2) {
                    expect(res2).to.have.status(200);
                    chai.request(server)
                        .get('/quizzes/'+results.quizOne.body._id)
                        .end(function (err, res) {
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('questions');
                            expect(res.body.questions).to.not.be.empty;
                            expect(res.body.questions[0]).to.have.property('title');
                            expect(res.body.questions[0]).to.have.property('answers');
                            done();
                        });
                });
        });
    });

    it('should create a quiz with questions', function (done) {
        async.parallel({
            questionOne: function (cb) { chai.request(server).post('/questions').send({title: 'Question title 20'}).end(cb); },
            questionTwo: function (cb) { chai.request(server).post('/questions').send({title: 'Question title 21'}).end(cb); }
        }, function (err, results) {
            expect(results.questionOne).to.have.status(200);
            expect(results.questionOne.body).to.have.property('title', 'Question title 20');
            expect(results.questionTwo).to.have.status(200);
            expect(results.questionTwo.body).to.have.property('title', 'Question title 21');

            chai.request(server)
                .post('/quizzes')
                .send({
                    title: 'Quiz title 20',
                    questions: [
                        results.questionOne.body._id,
                        results.questionTwo.body._id
                    ]
                })
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('questions');
                    expect(res.body.questions).to.not.be.empty;
                    done();
                });
        });
    });

    it('shold create a quiz with questions, which has answers', function (done) {
        async.parallel({
            answerOne: function (cb) { chai.request(server).post('/answers').send({title: 'Answer title 20'}).end(cb); },
            answerTwo: function (cb) { chai.request(server).post('/answers').send({title: 'Answer title 21'}).end(cb); },
            answerThree: function (cb) { chai.request(server).post('/answers').send({title: 'Answer title 22'}).end(cb); },
            answerFour: function (cb) { chai.request(server).post('/answers').send({title: 'Answer title 23'}).end(cb); }
        }, function (err, results) {
            async.parallel({
                questionOne: function (cb) {
                    chai.request(server).post('/questions')
                        .send({title: 'Question title 20', answers: [results.answerOne.body._id, results.answerTwo.body._id], correct: results.answerOne.body._id})
                        .end(cb);
                },
                questionTwo: function (cb) {
                    chai.request(server).post('/questions')
                        .send({title: 'Question title 21', answers: [results.answerThree.body._id, results.answerFour.body._id], correct: results.answerThree.body._id}).end(cb);
                }
            }, function (err, results) {

                expect(results.questionOne).to.have.status(200);
                expect(results.questionOne.body).to.have.property('title', 'Question title 20');
                expect(results.questionOne.body).to.have.property('answers');
                expect(results.questionOne.body.answers).to.not.be.empty;
                expect(results.questionTwo).to.have.status(200);
                expect(results.questionTwo.body).to.have.property('title', 'Question title 21');

                chai.request(server)
                    .post('/quizzes')
                    .send({
                        title: 'Quiz title 30',
                        questions: [
                            results.questionOne.body._id,
                            results.questionTwo.body._id
                        ]
                    })
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        chai.request(server)
                            .get('/quizzes/'+res.body._id)
                            .end(function (err, res) {
                                expect(res).to.have.status(200);
                                expect(res.body).to.have.property('questions');
                                expect(res.body.questions).to.not.be.empty;
                                expect(res.body.questions[0]).to.have.property('correct');
                                expect(res.body.questions[0].correct).to.have.property('title');
                                expect(res.body.questions[0]).to.have.property('answers');
                                done();
                            });

                    });
            });
        });
    });

});