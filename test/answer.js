const answer = require('../models/answer');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const expect = chai.expect;

chai.use(chaiHttp);
chai.config.includeStack = true;

describe('Answer API', function () {
    beforeEach(function (done) {
        answer.remove({}, function (err) {
            answer.create({
                title: 'Test answer'
            }, function (err) {
                done();
            });
        });
    });

    it('should create a answer', function (done) {
        chai.request(server)
            .post('/answers')
            .send({
                title: 'Answer title'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should return answers', function (done) {
        chai.request(server)
            .get('/answers')
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('object');
                expect(res.body).to.have.property('docs');
                expect(res.body.docs).to.not.be.empty;
                expect(res.body).to.have.property('total', 1);
                done();
            });
    });

    it('should return no answers on empty page', function (done) {
        chai.request(server)
            .get('/answers?page=2')
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('object');
                expect(res.body).to.have.property('docs');
                expect(res.body.docs).to.be.empty;
                expect(res.body).to.have.property('total', 1);
                done();
            });
    });

    it('should create and delete answer', function (done) {
        chai.request(server)
            .post('/answers')
            .send({
                title: 'Answer title 2'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                chai.request(server)
                    .delete('/answers/' + res.body._id)
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        done();
                    });
            });
    });

    it('should create and edit answer', function (done) {
        chai.request(server)
            .post('/answers')
            .send({
                title: 'Answer title 3'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('title', 'Answer title 3');
                chai.request(server)
                    .put('/answers/' + res.body._id)
                    .send({
                        title: 'Answer title 4'
                    })
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        done();
                    });
            });
    });


});