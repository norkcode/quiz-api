// const mongoose = require('mongoose');
// const account = require('../models/account');
//
// const chai = require('chai');
// const chaiHttp = require('chai-http');
// const server = require('../app');
// const should = chai.should();
//
// chai.use(chaiHttp);
//
// describe('account', function() {
//     beforeEach(function (done) {
//         account.remove({}, function (err) {
//             done();
//         });
//     });
//
//     describe('POST / account', function () {
//         it('should create an account', function (done) {
//             chai.request(server)
//                 .post('/users')
//                 .send({
//                     username: 'username',
//                     password: 'password'
//                 })
//                 .end(function (err, res) {
//                     res.should.have.status(200);
//                     res.body.should.be.a('object');
//                     res.body.should.have.property('username');
//                     done();
//                 });
//         });
//         it('should return error then no username is present', function(done) {
//             chai.request(server)
//                 .post('/users')
//                 .send({
//                     password: 'password'
//                 })
//                 .end(function (err, res) {
//                     res.should.have.status(400);
//                     res.body.should.have.property('message');
//                     done();
//                 });
//         });
//
//     });
//
//     describe('POST /login account', function() {
//         it('should create an account and login', function (done) {
//             chai.request(server)
//                 .post('/users')
//                 .send({
//                     username: 'username',
//                     password: 'password'
//                 })
//                 .end(function (err, res) {
//                     res.should.have.status(200);
//                     chai.request(server)
//                         .post('/users/login')
//                         .send({
//                             username: 'username',
//                             password: 'password'
//                         })
//                         .end(function (err, res) {
//                             console.log(res.body);
//                             res.should.have.status(200);
//                             done();
//                         });
//                 });
//         });
//     });
// });