const question = require('../models/question');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const expect = chai.expect;

chai.use(chaiHttp);
chai.config.includeStack = true;

describe('Question API', function () {
    beforeEach(function (done) {
        question.remove({}, function (err) {
            question.create({
                title: 'Test question'
            }, function (err) {
                done();
            });
        });
    });

    it('should create a question', function (done) {
        chai.request(server)
            .post('/questions')
            .send({
                title: 'Question title'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('object');
                expect(res.body).to.have.property('_id');
                expect(res.body).to.have.property('title', 'Question title');
                done();
            });
    });

    it('should return questions', function (done) {
        chai.request(server)
            .get('/questions')
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('object');
                expect(res.body).to.have.property('docs');
                expect(res.body.docs).to.not.be.empty;
                expect(res.body).to.have.property('total', 1);
                done();
            });
    });

    it('should return no questions on empty page', function (done) {
        chai.request(server)
            .get('/questions?page=2')
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('object');
                expect(res.body.docs).to.be.empty;
                done();
            });
    });

    it('should create and delete question', function (done) {
        chai.request(server)
            .post('/questions')
            .send({
                title: 'Question title 2'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                chai.request(server)
                    .delete('/questions/' + res.body._id)
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        done();
                    });
            });
    });

    it('should create and edit question', function (done) {
        chai.request(server)
            .post('/questions')
            .send({
                title: 'Question title 3'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('title', 'Question title 3');
                chai.request(server)
                    .put('/questions/' + res.body._id)
                    .send({
                        title: 'Question title 4'
                    })
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        expect(res.body).not.to.have.property('title');
                        done();
                    });
            });
    });

    it('should create question with answer', function (done) {
        chai.request(server)
            .post('/answers')
            .send({
                title: 'Answer title 1'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('_id');
                chai.request(server)
                    .post('/questions')
                    .send({
                        title: 'Question title 1',
                        answers: [
                            res.body._id
                        ],
                        correct: res.body._id
                    })
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('answers');
                        expect(res.body.answers).to.not.be.empty;
                        expect(res.body).to.have.property('correct');
                        done();
                    });
            });
    });

    it('should create question and update with answer', function (done) {
        chai.request(server)
            .post('/answers')
            .send({
                title: 'Answer title 2'
            })
            .end(function (err, answerRes) {
                expect(answerRes).to.have.status(200);
                chai.request(server)
                    .post('/questions')
                    .send({
                        title: 'Question title 9'
                    })
                    .end(function (err, res) {
                        expect(res).to.have.status(200);
                        chai.request(server)
                            .put('/questions/'+res.body._id)
                            .send({
                                title: res.body.title,
                                answers: [answerRes.body._id],
                                correct: answerRes.body._id
                            })
                            .end(function (err, res2) {
                                expect(res2).to.have.status(200);
                                chai.request(server)
                                    .get('/questions/'+res.body._id)
                                    .end(function (req, res) {
                                        expect(res).to.have.status(200);
                                        expect(res.body).to.have.property('answers');
                                        expect(res.body.answers).to.be.not.empty;
                                        expect(res.body.answers[0]).to.have.property('title');
                                        expect(res.body).to.have.property('correct');
                                        expect(res.body.correct).to.have.property('title');
                                        done();
                                    });
                            });
                    });
            });
    });
});